## Solarized Browser Homepage 
This is a simple browser start page using the Solarized color palette. It features a welcome message, real-time clock and a search bar.

## For best results
- The Ubuntu Regular and Ubuntu Light fonts should be installed for the page to display correctly.

![example image](./docs/example.png)
