ifeq ($(PREFIX),)
    PREFIX := $(HOME)
endif

install:
	install -d $(DESTDIR)$(PREFIX)/.homepage/
	install -m 755 src/index.html $(DESTDIR)$(PREFIX)/.homepage/ 

uninstall:
	rm -rf $(DESTDIR)$(PREFIX)/.homepage

